import Route from '@ember/routing/route';
import { service } from '@ember/service';
import ENV from '../config/environment';

export default class HomeRoute extends Route {
    @service store;
    model() {
        let url = ENV.projectIp + '/passbook';
        let request = new XMLHttpRequest();
        request.open('POST', url);
        fetch(ENV.projectIp + '/logincheck', { method: 'POST' }).then(
            (response) => {
                response.json().then((resp) => {
                    if (resp.login === 'false') {
                        window.location.hash = '#/';
                    } else {
                        request.onload = () => {
                            if (request.status === 200 || request.readyState === 4) {
                                let userDetails = JSON.parse(request.response);
                                this.store.pushPayload({
                                    passbook: userDetails.passbook,
                                });
                            }
                        };
                        request.setRequestHeader(
                            'Content-type',
                            'application/x-www-form-urlencoded'
                        );
                        request.send();
                    }
                });
            }
        );
    }
}