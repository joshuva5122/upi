import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import ENV from '../config/environment';

export default class IndexController extends Controller {
  @tracked signinUsername = '';

  @tracked signupUsername = '';

  @tracked signinPassword = '';

  @tracked signupPassword = '';

  @tracked loginFailed = false;

  @tracked signupFailed = false;

  @action login() {
    let url = ENV.projectIp + '/login';
    let request = new XMLHttpRequest();
    request.onload = () => {
      if (request.status === 200 || request.readyState === 4) {
        if (JSON.parse(request.response).login == 'success') {
          window.location.hash = '#/home';
        } else {
          this.loginFailed = true;
        }
      }
    };
    request.open('POST', url);
    request.setRequestHeader(
      'Content-type',
      'application/x-www-form-urlencoded'
    );
    request.send(
      `data={"signinUsername" : "${this.signinUsername}", "signinPassword" : "${this.signinPassword}"}`
    );
  }

  @action signup() {
    let url = ENV.projectIp + '/signup';
    let request = new XMLHttpRequest();
    request.onload = () => {
      if (request.status === 200 || request.readyState === 4) {
        if (JSON.parse(request.response).signup == 'success') {
          window.location.hash = '#/home';
        } else {
          this.signupFailed = true;
        }
      }
    };
    request.open('POST', url);
    request.setRequestHeader(
      'Content-type',
      'application/x-www-form-urlencoded'
    );
    request.send(
      `data={"signupUsername" : "${this.signupUsername}", "signupPassword" : "${this.signupPassword}"}`
    );
  }
}
