import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import fetch from 'fetch';
import { computed } from '@ember/object';
import { service } from '@ember/service';
import ENV from '../config/environment';

export default class HomeController extends Controller {

    @service store;

    @tracked userName = '';

    @tracked upiID = ' ';

    @tracked balance = '';

    @tracked verify = true;

    @tracked setAmount = false;

    @tracked transferMoney = false;

    @tracked msg = '';

    @tracked toUpi = '';

    @tracked amount = '';

    @tracked password = '';

    @tracked btn = true;

    @tracked showDetails = false;

    @tracked showTransactions = false;

    @tracked transactionBtn = 'Show Transactions';

    @tracked transDetails = "";

    @tracked accChange = [];

    @tracked updatePass = [];

    @action accountDetails() {
        fetch(ENV.projectIp + '/userdetails', { method: 'POST' }).then(
            (response) => {
                response.json().then((resp) => {
                    this.userName = resp.username;
                    this.upiID = resp.upiid;
                    this.balance = resp.balance;
                });
            }
        );
    }

    @action verifyUPI() {
        let url = ENV.projectIp + '/verifyupi';
        let request = new XMLHttpRequest();
        request.onload = () => {
            if (request.status === 200 || request.readyState === 4) {
                if (JSON.parse(request.response).upi == 'success') {
                    this.msg = '';
                    this.verify = false;
                    this.setAmount = true;
                } else {
                    this.msg = 'Enter valid UPI ID';
                }
            }
        };
        request.open('POST', url);
        request.setRequestHeader(
            'Content-type',
            'application/x-www-form-urlencoded'
        );
        request.send(`data={"upiID" : "${this.toUpi}"}`);
    }

    @action amountSet() {
        this.msg = '';
        this.setAmount = false;
        this.transferMoney = true;
    }

    @action transferAmount() {
        let url = ENV.projectIp + '/transferAmount';
        let request = new XMLHttpRequest();
        request.onload = () => {
            if (request.status === 200 || request.readyState === 4) {
                if (JSON.parse(request.response).transfer == 'success') {
                    this.toUpi = '';
                    this.verify = true;
                    this.setAmount = false;
                    this.transferMoney = false;
                    alert('Transaction success');
                    location.reload();
                } else {
                    this.msg = JSON.parse(request.response).transfer;
                }
            }
        };
        request.open('POST', url);
        request.setRequestHeader(
            'Content-type',
            'application/x-www-form-urlencoded'
        );
        if (this.amount == 0) {
            this.amount = 1;
        }
        request.send(
            `data={"fromupi" : "${this.upiID}", "toupi" : "${this.toUpi}", "password" : "${this.password}", "amount" : "${this.amount}"}`
        );
    }

    @computed('toUpi')
    get sendTo() {
        if (this.toUpi === this.upiID) {
            this.msg = "It's your's";
            this.btn = false;
        } else {
            this.msg = '';
            this.btn = true;
        }
    }

    @computed('amount')
    get amt() {
        if (this.amount < 0) {
            this.amount *= -1;
        } else if (this.amount > 10000) {
            alert('maximum amount is 10000');
            this.amount = 10000;
        }
    }

    @action logout() {
        fetch(ENV.projectIp + '/logout', { method: 'POST' });
        location.reload();
    }

    @action loginCheck() {
        fetch(ENV.projectIp + '/logincheck', { method: 'POST' }).then(
            (response) => {
                response.json().then((resp) => {
                    if (resp.login === 'false') {
                        window.location.hash = '#/';
                    } else {
                        this.accountDetails();
                    }
                });
            }
        );
    }

    @action userIcon() {
        this.accountDetails();
        this.showDetails ^= true;
    }

    @action transactionButton() {
        this.transDetails = this.storeData();
        this.showTransactions ^= true;
        if (this.showTransactions) {
            this.transactionBtn = 'Hide Transactions';
        } else {
            this.transactionBtn = 'Show Transactions';
        }
    }

    @action storeData() {
        return this.store.peekAll('passbook').find((element) => {
            return (element.myId = this.upiID);
        });
    }

    @action changeAccount() {
        this.accChange[0] = prompt("Enter username: ");
        this.accChange[1] = prompt("Enter Password: ");
        let url = ENV.projectIp + '/login';
        let request = new XMLHttpRequest();
        request.onload = () => {
            if (request.status === 200 || request.readyState === 4) {
                if (JSON.parse(request.response).login == 'success') {
                    alert("Login Success");
                    location.reload();
                } else {
                    alert("Login Failed");
                }
            }
        };
        request.open('POST', url);
        request.setRequestHeader(
            'Content-type',
            'application/x-www-form-urlencoded'
        );
        request.send(
            `data={"signinUsername" : "${this.accChange[0]}", "signinPassword" : "${this.accChange[1]}"}`
        );
    }

    @action updatePassword() {
        this.updatePass[0] = prompt("Enter Current Password: ");
        this.updatePass[1] = prompt("Enter New Password: ");
        let url = ENV.projectIp + '/updatepassword';
        let request = new XMLHttpRequest();
        request.onload = () => {
            if (request.status === 200 || request.readyState === 4) {
                if (JSON.parse(request.response).updatePassword == 'success') {
                    alert("Password Updated");
                } else {
                    alert("Password Update Failed");
                }
            }
        };
        request.open('POST', url);
        request.setRequestHeader(
            'Content-type',
            'application/x-www-form-urlencoded'
        );
        request.send(
            `data={"username" : "${this.userName}", "oldPassword" : "${this.updatePass[0]}", "newPassword" : "${this.updatePass[1]}"}`
        );
    }
}