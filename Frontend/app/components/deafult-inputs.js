import Component from '@glimmer/component';
import { computed } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class DeafultInputsComponent extends Component {
  @tracked showBtn = false;

  @computed('args.username')
  get checkUsername() {
    var regex = new RegExp('^[a-zA-Z]+$');
    if (regex.test(this.args.username)) {
      this.showBtn = true;
      return 'black';
    } else {
      this.showBtn = false;
      return 'red';
    }
  }
}
