import Model, { attr } from '@ember-data/model';

export default class PassbookModel extends Model {
  @attr myId;
  @attr toUpi;
  @attr status;
  @attr toAmount;
  @attr toDateAndTime;
  @attr fromUpi;
  @attr fromAmount;
  @attr fromDateAndTime;
}
