import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse, HttpSession, Cookie}
import java.io.PrintWriter

class LoginCheck extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val session: HttpSession  = request.getSession()
        val printwriter: PrintWriter = new PrintWriter(response.getWriter)
        val logedin: Boolean = session.getAttribute("uname") == null
        if (logedin) printwriter.print("""{"login" : "false"}""") else printwriter.print("""{"login" : "true"}""")
    }
}