import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse, HttpSession}
import org.json.JSONObject
import java.io.PrintWriter

class UserDetails extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val printwriter: PrintWriter = new PrintWriter(response.getWriter)
        val session: HttpSession  = request.getSession();
        val db: DBfunc = new DBfunc()
        val details: JSONObject = db.userData(session.getAttribute("uname").toString)
        details.put("id", "1")
        printwriter.print(details)
    }
}