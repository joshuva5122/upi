import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse, HttpSession}
import org.json.JSONObject
import java.io.PrintWriter

class PassBook extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val session: HttpSession  = request.getSession()
        val printwriter: PrintWriter = new PrintWriter(response.getWriter)
        val db: DBfunc = new DBfunc
        db.connectDB
        val upiid: String = db.getData(session.getAttribute("uname").toString, "upiid").replace(" ", "")
        val uId: String = db.getData(session.getAttribute("uname").toString, "uid")
        val amountSent: JSONObject = db.userTransactionsSent(upiid, session.getAttribute("uname").toString)
        val amountReceived: JSONObject = db.userTransactionsReceived(upiid, session.getAttribute("uname").toString)
        db.closeDB
        printwriter.println(s"""{"passbook" : [$amountSent, $amountReceived, {"id" : "$uId", "myId" : "$upiid"}]}""")
    }
}