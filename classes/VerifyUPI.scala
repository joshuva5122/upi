import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import java.io.PrintWriter
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

class VerifyUPI extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val data: String = request.getParameter("data")
        val parser: JSONParser = new JSONParser()
        val jsonData: JSONObject = parser.parse(data).asInstanceOf[JSONObject]
        val printwriter: PrintWriter = new PrintWriter(response.getWriter)
        val db: DBfunc = new DBfunc()
        val upiID: String = jsonData.get("upiID").toString
        db.connectDB
        if(db.checkData(data = upiID, table = "users", column = "upiid")) {
            printwriter.print("""{"upi" : "success"}""")
        } else {
            printwriter.print("""{"upi" : "failed"}""")
        }
        db.closeDB 
    }
}