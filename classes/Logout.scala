import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse, HttpSession}

class Logout extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val session: HttpSession  = request.getSession()
        session. invalidate()
    }
}