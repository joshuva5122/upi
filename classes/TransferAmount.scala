import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse, HttpSession, Cookie}
import java.io.PrintWriter
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class TransferAmount extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val data: String = request.getParameter("data")
        val parser: JSONParser = new JSONParser()
        val session: HttpSession  = request.getSession();
        val jsonData: JSONObject = parser.parse(data).asInstanceOf[JSONObject]
        val fromUpi: String = jsonData.get("fromupi").toString
        val toUpi: String = jsonData.get("toupi").toString
        val password: String = jsonData.get("password").toString
        val amount: Double = jsonData.get("amount").toString.toDouble
        val db: DBfunc = new DBfunc()
        val printwriter: PrintWriter = new PrintWriter(response.getWriter)
        val dateTime: LocalDateTime = LocalDateTime.now()
        val formater: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy'-'HH:mm:ss")
        val transTime: String = dateTime.format(formater)
        if(db.login(username = fromUpi, password = password, column = "upiid")) {
            db.connectDB
            if (db.checkData(data = toUpi, table = "users", column = "upiid") && ! fromUpi.equals(toUpi)) {
                if ((1000 - db.getData(session.getAttribute("uname").toString, "dailylimit").toDouble) >= amount) {
                    if (amount <= db.getBalance(fromUpi)) {
                            //success
                            db.updateBalance(fromUpi, "- " + amount)
                            db.updateBalance(toUpi, "+ " + amount)
                            db.transactions(fromUpi, toUpi, amount, "success", transTime)
                            db.dailyLimit(fromUpi, amount)
                            printwriter.println("""{"transfer" : "success"}""")
                    } else {
                            // insufficient fund
                            db.transactions(fromUpi, toUpi, amount, "insufficient fund", transTime)
                            printwriter.println("""{"transfer" : "insufficient fund"}""")
                    }
                } else {
                        //exceeded limit
                        db.transactions(fromUpi, toUpi, amount, "exceeded limit", transTime)
                        printwriter.println("""{"transfer" : "exceeded limit"}""")
                }
            } else {
                // upi not found
                db.transactions(fromUpi, toUpi, amount, "upi not found", transTime)
                printwriter.println("""{"transfer" : "upi not found",}""")
            }
        } else {
            // auth failed
            db.connectDB
            db.transactions(fromUpi, toUpi, amount, "auth failed", transTime)
            printwriter.println("""{"transfer" : "auth failed"}""")
        }
        db.closeDB
    }
}