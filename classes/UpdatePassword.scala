import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import java.io.PrintWriter
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

class UpdatePassword extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val data: String = request.getParameter("data")
        val parser: JSONParser = new JSONParser()
        val jsonData: JSONObject = parser.parse(data).asInstanceOf[JSONObject]
        val username: String = jsonData.get("username").toString
        val oldPassword: String = jsonData.get("oldPassword").toString
        val newPassword: String = jsonData.get("newPassword").toString
        val db: DBfunc = new DBfunc()
        val printwriter: PrintWriter = new PrintWriter(response.getWriter)
        if (db.login(username = username, password = oldPassword, column = "uname")) {
            db.updatePassword(username, newPassword)
            printwriter.print("""{"updatePassword" : "success"}""")
        } else {
            printwriter.print("""{"updatePassword" : "failed"}""")
        }
    }
}