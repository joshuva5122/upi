import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse, HttpSession}
import java.io.PrintWriter
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser

class Login extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val data: String = request.getParameter("data")
        val parser: JSONParser = new JSONParser()
        val session: HttpSession  = request.getSession()
        val jsonData: JSONObject = parser.parse(data).asInstanceOf[JSONObject]
        val username: String = jsonData.get("signinUsername").toString
        val password: String = jsonData.get("signinPassword").toString
        val db: DBfunc = new DBfunc()
        val printwriter: PrintWriter = new PrintWriter(response.getWriter)
        if (db.login(username = username, password = password, column = "uname")) {
            session.setAttribute("uname", username)
            printwriter.print("""{"login" : "success"}""")
        } else {
            printwriter.print("""{"login" : "failed"}""")
        }
    }
}