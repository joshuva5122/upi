import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse, HttpSession, Cookie}
import java.io.PrintWriter
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.net.HttpCookie

class Signup extends HttpServlet {
    override def doPost(request: HttpServletRequest, response: HttpServletResponse): Unit = {
        response.addHeader("Access-Control-Allow-Origin", "*")
        val data: String = request.getParameter("data")
        val parser: JSONParser = new JSONParser()
        val session: HttpSession  = request.getSession();
        val jsonData: JSONObject = parser.parse(data).asInstanceOf[JSONObject]
        val username: String = jsonData.get("signupUsername").toString()
        val password: String = jsonData.get("signupPassword").toString()
        val db: DBfunc = new DBfunc()
        val printwriter: PrintWriter = new PrintWriter(response.getWriter)
        if (db.signup(username = username, password = password)) {
            session.setAttribute("uname", username)
            printwriter.print("""{"signup" : "success"}""")
        } else {
            printwriter.print("""{"signup" : "failed"}""")
        }
    }
}