import java.sql.{Connection, DriverManager, PreparedStatement, ResultSet}
import org.json.{JSONObject, JSONArray}

class DBfunc {

    var connection: Connection = null

    def connectDB(): Unit = {
        Class.forName("org.postgresql.Driver")
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/upi", "josh", "josh")
    }

    def closeDB(): Unit = {
        connection.close
    }

    def checkData(data: String, table: String, column: String): Boolean = {
        val preparedstatement:PreparedStatement = connection.prepareStatement("select " + column + " from " + table + " where " + column + " = ?;")
        preparedstatement.setString(1, data)
        val resultset: ResultSet = preparedstatement.executeQuery()
        resultset.next
		if(resultset.getRow() >= 1 && resultset.getString(column).replace(" ", "").equals(data)){
			true
		} else {
            false
        } 
    }

    def checkDataPass(data: String, table: String, column1: String, column2: String): String = {
        val preparedstatement:PreparedStatement = connection.prepareStatement("select " + column1 + " from " + table + " where " + column2 + " = ?;")
        preparedstatement.setString(1, data)               
        val resultset: ResultSet = preparedstatement.executeQuery()
        resultset.next()
		resultset.getString(column1).replace(" ", "")
    }

    def login(username: String, password: String, column: String): Boolean = {
        connectDB                                                                     
        if (checkData(data = username, table = "users", column = column) && checkDataPass(data = username, table = "users", column1 = "password", column2 = column).equals(password)) {
            closeDB
            true
        } else {
            closeDB
            false
        }
    }

    def signup(username: String, password: String): Boolean = {
        connectDB
        if (!checkData(data = username, table = "users", column = "uname")) {
            val preparedstatement: PreparedStatement = connection.prepareStatement("insert into users(uname, password, upiid, balance) values(?, ?, ?, ?);");
			preparedstatement.setString(1, username);
			preparedstatement.setString(2, password);
            preparedstatement.setString(3, username + "@okupi");
            preparedstatement.setInt(4, 0);
			preparedstatement.executeUpdate();
            closeDB
            true
        } else {
            closeDB
            false
        }
    }

    def userData(username: String): JSONObject = {
        connectDB
        val jsonObject: JSONObject = new JSONObject
        val preparedstatement: PreparedStatement = connection.prepareStatement("select * from users where uname = ?;")
        preparedstatement.setString(1, username)
        val resultset: ResultSet = preparedstatement.executeQuery()
        resultset.next()
        jsonObject.put("username", resultset.getString("uname").replace(" ", ""))
        jsonObject.put("upiid", resultset.getString("upiid").replace(" ", ""))
        jsonObject.put("balance", resultset.getDouble("balance").toString)
        closeDB
        jsonObject
    }

    def getBalance(upi: String): Double = {
        val preparedstatement:PreparedStatement = connection.prepareStatement("select balance from users where upiid = ?;")
        preparedstatement.setString(1, upi)
        val resultset: ResultSet = preparedstatement.executeQuery()
        resultset.next()
		resultset.getDouble("balance")
    }

    def updateBalance(upi: String, change: String): Unit = {
        val preparedstatement:PreparedStatement = connection.prepareStatement("update users set balance = balance " + change + " where upiid = ?")
        preparedstatement.setString(1, upi)
        preparedstatement.executeUpdate()
    }

    def transactions(fromupi: String, toupi: String, amount: Double, status: String, dateTime: String): Unit = {
        val preparedstatement:PreparedStatement = connection.prepareStatement("insert into transactions(fromupi, toupi, amount, status, dateandtime) values(?, ?, ?, ?, ?);")
        preparedstatement.setString(1, fromupi)
        preparedstatement.setString(2, toupi)
        preparedstatement.setDouble(3, amount)
        preparedstatement.setString(4, status)
        preparedstatement.setString(5, dateTime)
        preparedstatement.executeUpdate()
    }

    def getData(uname: String, column: String): String = {
        val preparedstatement:PreparedStatement = connection.prepareStatement("select "+ column + " from users where uname = ?;")
        preparedstatement.setString(1, uname)
        val resultset: ResultSet = preparedstatement.executeQuery()
        resultset.next
        resultset.getString(column)
    }


    def userTransactionsSent(upi: String, uname: String): JSONObject = {
        connectDB
        val jsonObject: JSONObject = new JSONObject
        val toupi: JSONArray = new JSONArray
        val amount: JSONArray = new JSONArray
        val status: JSONArray = new JSONArray
        val dateandtime: JSONArray = new JSONArray
        val uId: String = getData(uname, "uid")
        val preparedstatement: PreparedStatement = connection.prepareStatement("select * from transactions where fromupi = ? order by tid DESC;")
        preparedstatement.setString(1, upi)
        val resultset: ResultSet = preparedstatement.executeQuery()
        while(resultset.next) {
            toupi.put(resultset.getString("toupi").replace(" ", ""))
            amount.put(resultset.getString("amount").replace(" ", ""))
            status.put(resultset.getString("status").replace(" ", ""))
            dateandtime.put(resultset.getString("dateandtime").replace(" ", ""))
        }
        closeDB
        jsonObject.put("id", uId)
        jsonObject.put("toUpi", toupi)
        jsonObject.put("toAmount", amount)
        jsonObject.put("status", status)
        jsonObject.put("toDateAndTime", dateandtime)
        jsonObject
    } 

    def userTransactionsReceived(upi: String, uname: String): JSONObject = {
        connectDB
        val jsonObject: JSONObject = new JSONObject
        val fromupi: JSONArray = new JSONArray
        val amount: JSONArray = new JSONArray
        val status: JSONArray = new JSONArray
        val dateandtime: JSONArray = new JSONArray
        val uId: String = getData(uname, "uid")
        val preparedstatement: PreparedStatement = connection.prepareStatement("select fromupi, amount, dateandtime from transactions where toupi = ? order by tid DESC;")
        preparedstatement.setString(1, upi)
        val resultset: ResultSet = preparedstatement.executeQuery()
        while(resultset.next) {
            fromupi.put(resultset.getString("fromupi").replace(" ", ""))
            amount.put(resultset.getString("amount").replace(" ", ""))
            dateandtime.put(resultset.getString("dateandtime").replace(" ", ""))
        }
        closeDB
        jsonObject.put("id", uId)
        jsonObject.put("fromUpi", fromupi)
        jsonObject.put("fromAmount", amount)
        jsonObject.put("fromDateAndTime", dateandtime)
        jsonObject
    }

    def dailyLimit(upiid: String, amount: Double): Unit = {
        val preparedstatement:PreparedStatement = connection.prepareStatement("update users set dailylimit = ? where upiid = ?;")
        preparedstatement.setDouble(1, amount)
        preparedstatement.setString(2, upiid)
        preparedstatement.executeUpdate
    }

    def updatePassword(uname: String, newPassword: String): Unit = {
        connectDB
        val preparedstatement:PreparedStatement = connection.prepareStatement("update users set password = ? where uname = ?;")
        preparedstatement.setString(1, newPassword)
        preparedstatement.setString(2, uname)
        preparedstatement.executeUpdate
        closeDB
    }
}